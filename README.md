# Online Annotation Tools

`word_alignment` allows the collection of word-level alignment information from parallel data.

`transcription` is a casestudy tool, that provides the user with audio segments and translations
in another language, along with (gold or noisy) speech-to-translation alignment information.

The `transcription` tool was used in the case study that was presented at ComputEL-2.
If you use it, please cite

> [A case study on using speech-to-translation alignments for language documentation](https://arxiv.org/pdf/1702.04372.pdf)

> [Antonis Anastasopoulos](https://www3.nd.edu/~aanastas/) and [David Chiang](https://www3.nd.edu/~dchiang/)

> [(bibtex)](https://www3.nd.edu/~aanastas/research/computel-anastasopoulos.bib)

