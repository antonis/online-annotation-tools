The first page is `casestudy.php`.

Depending on which mode it is called with, the user is presented with:

* mode 0: no speech-to-translation alignment information

* mode 1: Gold speech-to-translation alignment information (from `sample_data/gal-data`)

* mode 2: Noisy speech-to-translation alignment information (from `sample_data/al-data`)

The `js/my-init.js` initializes the audio and translation components with the necessary data.

The user can not only provide a transcription of the audio, but also drag and resize the alignments,
in order to correct them. Clicking on each alignment word will play the corresponding audio segment.

The form saves in a file (in this example in `sample_data/outputs/user1.txt`)
the following information for each segment, tab-separated:

* time the user started working on this segment

* segment id

* corresponding audio filename

* sequence of clicks the user performed to listen to the segment: 
	`1` for clicking to play the whole audio, 
	`2` for clicking on one of the translation words to head the corresponding segment.

* time the user finished working on that segment

* mode under which the user was operating under (0, 1, or 2)

* the produced transcription.

One could potentially also save the (possibly updated) alignment information (start, end)
for each translation word. 

A working example can be found [here](https://www3.nd.edu/~aanastas/alignments/).

