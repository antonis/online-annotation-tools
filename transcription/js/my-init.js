

var wavesurfer = WaveSurfer.create({
  container: '#waveform',
  waveColor: 'red',
  progressColor: 'purple'
});

function initWav(i){
	wavesurfer.load('sample_data/audio/'+i+'.wav');
}

var allTime = wavesurfer.getDuration();
var boxPos = $('#box').offset();
var boxWidth = $('#box').width();

$('#bw').text('boxw: '+ boxWidth);

function initBox(w,s,e){
	var words = w.split(" ");
	var starts = s.split(" ");
	var ends = e.split(" ");
	for (i = 0; i < words.length; i++) {
		var sid = "arr"+i;
		var width = (parseFloat(ends[i])-parseFloat(starts[i]))*boxWidth;
		var xst = parseFloat(starts[i])*boxWidth;
	    $('#box').append($("<div class='myResizable' id='"+sid+"' style='width:"+width+"px; left:"+xst+"px; font-size:large;'><a onclick='playSample("+sid+")'>"+words[i]+"</a></div>"));
	};	
}


$(function() {
	$('.myResizable').resizable({
		handles : "e,w",
		axis: "x",
		containment: '#box',
		resize: function(event, ui) {
		    $(this).css({
		        'left': parseInt(ui.position.left, 10) 
		    });
		},
		stop: function(event, ui) {
			var offset = $(this).offset();
            var xPos = offset.left;
            var yPos = offset.top;
            var width = $(this).width();
            var relStart = (xPos - boxPos.left)/boxWidth;
			var relEnd = (xPos + width - boxPos.left)/boxWidth;
		}
	});
});

$(function() {
	$('.myResizable').draggable({
	    containment: '#box',
	    axis: "x",
	    start: function(event, ui) {
	    	startPos = ui.helper.position();
	        isDraggingMedia = true;
	    },
	    stop: function(event, ui) {
	        isDraggingMedia = false;
	        
	        var offset = $(this).offset();
            var xPos = offset.left;
            var yPos = offset.top;
            var width = $(this).width();
            var relStart = (xPos - boxPos.left)/boxWidth;
			var relEnd = (xPos + width - boxPos.left)/boxWidth;
	    },
	});
});

$(function() {
	$('.myResizable').click(function(){
		if ( $(this).is('.ui-draggable-dragging') ) {
		      return;
		}
		// click action here
		var offset = $(this).offset();
		var xPos = offset.left;
		var yPos = offset.top;
		var width = $(this).width();

		var relStart = (xPos - boxPos.left)/boxWidth;
		var relEnd = (xPos + width - boxPos.left)/boxWidth;
	});

});

$(function() {
	$('#submit1').click(function(){
		var user = $("#myUser").html()
		var transcription = $('#transcription1').val();
		//$('#output').append(transcription+"<br>");
		/*var alignments = "";
		for (i = 0; i < words.length; i++) {
			var sid = "#arr"+i;
			var offset = $(sid).offset();
			var xPos = offset.left;
			var yPos = offset.top;
			var width = $(sid).width();

			var relStart = (xPos - boxPos.left)/boxWidth;
			var relEnd = (xPos + width - boxPos.left)/boxWidth;
			var align = relStart + " " + relEnd;
			//$('#output').append(align + "<br>");
			alignments += words[i] + " " + align + "<br>";
		}
		$('#output').append(alignments);*/
		var d = new Date();
		var date = timeConverter(d.getTime());
		//$('#output').append(date + "<br>");
		//var data = date + "<br>" + transcription + "<br>" + alignments;
		var seg = $("#mySegment").html()
		var mode = $("#myMode").html()
		var data = date + '-' + seg +'-'+ mode +'-'+ transcription;
		var xhr = new XMLHttpRequest();
		//xhr.open( 'post', 'saveAlignments.php?q='+data, true );
		xhr.open( 'post', '../saveAlignments.php?f='+user+'&q='+date +'&seg='+seg+'&m='+mode+'&t='+transcription, true );
		xhr.send();
		var newseg = parseInt(seg)+1;
		var newmode = (parseInt(mode)+1) % 3;
		//$('#output').append(ns + "<br>");
		if (newseg < 60) {
			window.location='casestudy.php?seg='+newseg+'&f='+user+'&mode='+newmode;
		}
		else {
			window.location='thankyou.html'; 
		}
	});
});			


function playSample(obj){
	var offset = $(obj).offset();
	var xPos = offset.left;
	var yPos = offset.top;
	var width = $(obj).width();

	var relStart = (xPos - boxPos.left)/boxWidth;
	var relEnd = (xPos + width - boxPos.left)/boxWidth;

	var allTime = wavesurfer.getDuration();
	var start=relStart*allTime;
	var end = relEnd*allTime;
	wavesurfer.seekTo(start);
	wavesurfer.play(start, end);
	var user = $("#myUser").html()
	var xhr = new XMLHttpRequest();
	xhr.open( 'post', '../writeSample.php?f='+user, true );
	xhr.send();
}

function writePlay(){
	var user = $("#myUser").html()
	var xhr = new XMLHttpRequest();
	xhr.open( 'post', '../writePlay.php?f='+user, true );
	xhr.send();
}


function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  //var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  var time = hour + ':' + min + ':' + sec ;
  return time;
}

