<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Speech-to-translation alignments</title>
    
    
    <!-- Audio stuff -->
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>

    <!-- My stylesheet -->
	<link rel="stylesheet" href="../style.css">    
 
  <!-- Draggable stuff-->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script src="../js/wavesurfer.js"></script>

  </head>

  <body>


<div class="intro">
	<!--<p>Produce the gloss of the audio. You are given the words of the translation, as well
		as their alignments, as produced by our model.</p>
	<p>You can click on each word to listen to the corresponding part of the audio.
	</p>-->
  <p>Produce an acoustic transcription of the given audio. Use any conventions you want
    for representing the sounds.</p><br><br>
	<!--<p>If you find that the alignments are wrong, you can adjust them by dragging them and by resizing the ends of the arrows both left and right (not required). </p>-->
</div>


<div class="zoom">
  <button class="btn btn-primary" onclick="wavesurfer.playPause();writePlay();">
    <i class="glyphicon glyphicon-play"></i>
    Play
  </button>
</div>
<div id="waveform" class="waveform"></div>

<?php
$audios = ['130', '178', '127'];
date_default_timezone_set('America/New_York');
$seg = $_GET["seg"];
$fname = $_GET["f"];

echo $seg.'<br><br>';

$fname = "sample_data/output/test.txt";

$t=time();
$time=date("H:i:s",$t);

file_put_contents($fname,"\n".$time."\t".$seg."\t".$audios[$seg]."\t",FILE_APPEND);

$mode = $_GET["mode"];
$trfile = fopen("sample_data/translations/".$audios[$seg].".words", "r");
$line = fgets($trfile);

echo '<div class="translation">',
	'<p style="text-align:justify;">',
		'<b>Translation: </b>'.trim($line).'<br><br>',
	'</p>',
'</div>';
# In mode=0, we don't provide speech-to-translation alignments
# In modes 1 and 2, we provide them, and the box is initialized with js
if ($mode > 0)
 echo '<div id="box"></div>';
?>

<div class="myform">
	<b>Transcription:</b> <input type="text" id="transcription1" name="transcription" style="width:75%; margin:auto;">
	<button class="btn btn-primary" style="margin:auto;" id="submit1"> 
  Save </button>
</div>
<!--onclick="saveAls();location.href='http://localhost/~antonis/alignments/seq1/al-2-2.html'">-->
  
<div id="output" style="width:75%; margin:auto"></div>

<?php
$seg = $_GET["seg"];
$mode = $_GET["mode"];
$user = $_GET["f"];
echo '<div id="mySegment" style="display: none;">'.$seg.'</div>';
echo '<div id="myMode" style="display: none;">'.$mode.'</div>';
echo '<div id="myUser" style="display: none;">'.$user.'</div>';
?>

<?php
$audios = ['130', '178', '127'];
$seg = $_GET["seg"];
if ($mode <= 1)
  # In this mode we provide gold speech-to-translation alignments
  $alfile = fopen("sample_data/gal-data/".$audios[$seg].".words", "r");
if ($mode == 2)
  # In this mode we provide noisy speech-to-translation alignments
  $alfile = fopen("sample_data/al-data/".$audios[$seg].".words", "r");
$words = "";
$starts = "";
$ends = "";

while(!feof($alfile)){
  $line = fgets($alfile);
  $spl = explode(" ", trim($line));
  $words = $words.$spl[0]." ";
  $starts=$starts.$spl[1]." ";
  $ends.=$spl[2]." ";
}
$words = substr($words,0,-1);
$starts = substr($starts,0,-1);
$ends = substr($ends,0,-1);
//echo $words;
//echo $starts;
//echo $ends;

echo '<script src="../js/my-init.js"></script>';
echo '<script type="text/javascript">initWav('.$audios[$seg].');</script>';
echo '<script type="text/javascript">initBox("'.trim($words).'","'.trim($starts).'","'.trim($ends).'");</script>';
?>
    
  </body>
</html>
