<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Speech-to-translation alignments</title>
    
    
    <!-- Audio stuff -->
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>

    <!-- My stylesheet -->
	<link rel="stylesheet" href="align-style.css">    
 
  <!-- Draggable stuff-->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
  </head>

  <body>


<div class="intro">
	<p>Produce the alignments for the given sentence pairs. 
    You can click on each square to denote an alignment between the words. 
    Both many-to-many and null alignments are allowed.</p>
</div>

<div class="myform" >
  <form  action='getAlignments.php'  method="post" style="text-align:center;">
    <?php include 'createTables.php';?>
    <input type="submit" id="button" value="Submit"/>
  </form>
</div>

<script>
$(document).ready(function() {
  //$('.rotate').css('height', $('.rotate').width());
  $('.rotate').css('height', '30px');
  $('.rotate').css('width', '30px');

});
</script>

</body>
</html>
