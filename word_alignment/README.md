
The first page is `align.php`, which calls `createTables.php`.

`createTables.php` reads the parallel sentences from pre-defined datapaths
(in this case the `sample_data`) and constructs the forms.

The form is submitted with `getAlignments.php`.

A working example can be found [here](https://www3.nd.edu/~aanastas/alignments/align.php)

