<?php

$tfile = fopen("sample_data/tagalog.txt", "r");
$efile = fopen("sample_data/english.txt", "r");

$counter = 0;
$output = "";


while(!feof($tfile)) {
	$tsent = fgets($tfile);
	$esent = fgets($efile);
	if (substr( $tsent, 0, 1 ) != "#") {
		$twords = explode(" ", $tsent);
		$ewords = explode(" ", $esent);
		
		$output .= "<table style='table-layout: fixed; margin:auto;'>";
		$output .= "<tr>";
		$output .= "<td></td>";
		foreach ($ewords as $key => $value) { 
	     	$output .= "<td class='rotate' style='height: 50px; width:30px; overflow-y:visible; overflow-x:hidden;'>". $value ."</td>";
		} 
		$output .= "</tr>";

		foreach ($twords as $key => $value) {
			$output .= "<tr><td style='text-align:right; padding-right:5px;'>". $value ."</td>";
			foreach ($ewords as $key2 => $value2) {
	    		//$output .= "<td> <input type='checkbox' name='sent{$counter}[]' id='test-".$counter."-".$key."-".$key2."'/><label for='test-".$counter."-".$key."-".$key2."'></label> </td>";
	    		$output .= "<td> <input type='checkbox' name='sent".$counter."[]' id='test-".$counter."-".$key."-".$key2."' value='".$key."-".$key2."'/><label for='test-".$counter."-".$key."-".$key2."'></label> </td>";
			} 
			$output .= "</tr>";		
		}

		$output .= "</table><br>";
		$counter += 1;
	}
	
}
echo $output;


fclose($tfile);
fclose($efile);



?>
